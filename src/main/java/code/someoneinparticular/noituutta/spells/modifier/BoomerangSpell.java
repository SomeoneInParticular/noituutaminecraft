package code.someoneinparticular.noituutta.spells.modifier;

import code.someoneinparticular.noituutta.spells.SpellProjectileEntity;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.Vec3d;

import static code.someoneinparticular.noituutta.SpellUtils.targetedAcceleration;

public class BoomerangSpell implements IModifierSpell {
    // Universal flyweight instance of this spell for use in-game
    public static final BoomerangSpell INSTANCE = new BoomerangSpell();

    // How quickly the spell should rebound back towards the caster
    protected static final float ACCELERATION_RATE = 0.25f;

    @Override
    public void modifySpellEntity(SpellProjectileEntity entity) {
        entity.addTrajectoryModifier(this::shiftTrajectory);
    }

    public void shiftTrajectory(SpellProjectileEntity spell) {
        // Check to see if the spell has an owner to actually orbit around
        Entity target = spell.getOwner();
        // If not, do nothing to this projectile
        if (target == null) return;

        // Calculate the additional velocity that should be applied to the projectile to
        Vec3d addVec = targetedAcceleration(spell.getPos(), target.getPos(), ACCELERATION_RATE);

        // Apply the additional velocity to the projectile
        spell.addVelocity(addVec.x, addVec.y, addVec.z);
    }
}
