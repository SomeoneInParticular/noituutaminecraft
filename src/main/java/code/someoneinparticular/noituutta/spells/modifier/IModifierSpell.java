package code.someoneinparticular.noituutta.spells.modifier;

import code.someoneinparticular.noituutta.spells.INoituuttaSpell;
import code.someoneinparticular.noituutta.spells.SpellProjectileEntity;

public interface IModifierSpell extends INoituuttaSpell {
    void modifySpellEntity(SpellProjectileEntity spellEntity);
}
