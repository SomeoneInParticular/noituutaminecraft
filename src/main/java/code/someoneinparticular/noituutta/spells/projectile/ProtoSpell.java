package code.someoneinparticular.noituutta.spells.projectile;

import code.someoneinparticular.noituutta.Noituutta;
import code.someoneinparticular.noituutta.SpellUtils;
import code.someoneinparticular.noituutta.particles.ModParticles;
import code.someoneinparticular.noituutta.spells.INoituuttaSpell;
import code.someoneinparticular.noituutta.spells.SpellProjectileEntity;
import code.someoneinparticular.noituutta.spells.modifier.IModifierSpell;
import net.fabricmc.fabric.api.object.builder.v1.entity.FabricEntityTypeBuilder;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.SpawnGroup;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.World;

import java.util.List;

public class ProtoSpell implements IProjectileSpell {
    // Universal flyweight instance of this spell for use in-game
    public static final ProtoSpell INSTANCE = new ProtoSpell();

    public static final EntityType<ProtoSpellProjectileEntity> PROJECTILE_TYPE = Registry.register(
            Registry.ENTITY_TYPE,
            Noituutta.generateID("proto_spell"),
            FabricEntityTypeBuilder.create(SpawnGroup.MISC, ProtoSpellProjectileEntity::create)
                    .dimensions(SpellUtils.cubeEntity(ProtoSpellProjectileEntity.getHitboxSize()))
                    .trackRangeBlocks(4).trackedUpdateRate(10).forceTrackedVelocityUpdates(true).build());

    static final float DEFAULT_DAMAGE = 4.0f;
    static final float DEFAULT_KNOCKBACK = 1.0f;
    static final float DEFAULT_PROJECTILE_SPEED = 2.5f;
    static final float DEFAULT_GRAVITY = 0.01f;

    public static final Identifier TEXTURE = Noituutta.generateID("textures/entity/proto_spell.png");

    @Override
    public SpellProjectileEntity generateEntity(World world, LivingEntity owner, List<INoituuttaSpell> spells) {
        var spellProjectile = new ProtoSpellProjectileEntity(world, owner);
        spellProjectile.setPos(owner.getX(), owner.getEyeY()-0.1, owner.getZ());
        for (INoituuttaSpell s : spells) {
            if (s instanceof IModifierSpell) {
                ((IModifierSpell) s).modifySpellEntity(spellProjectile);
            }
        }
        return spellProjectile;
    }

    public static class ProtoSpellProjectileEntity extends SpellProjectileEntity {
        public ProtoSpellProjectileEntity(EntityType<? extends SpellProjectileEntity> entityType, World world) {
            super(entityType, world);
            this.damage = DEFAULT_DAMAGE;
            this.knockback = DEFAULT_KNOCKBACK;
            this.gravity = DEFAULT_GRAVITY;
        }

        // Wrapper for the prior constructor, because Java has a stroke for some reason if we don't
        public static ProtoSpellProjectileEntity create(EntityType<? extends SpellProjectileEntity> entityType, World world) {
            return new ProtoSpellProjectileEntity(entityType, world);
        }

        public ProtoSpellProjectileEntity(World world, LivingEntity caster) {
            this(PROJECTILE_TYPE, world);
            // Orient the projectile in the direction the caster is facing, and spawn it with the corrected speed
            this.setProperties(caster, caster.getPitch(), caster.getYaw(), caster.getRoll(), this.getCastSpeed(), 0);
            this.setOwner(caster);
        }

        @Override
        protected void initDataTracker() {
        }

        @Override
        public float getCastSpeed() {
            return DEFAULT_PROJECTILE_SPEED;
        }

        @Override
        public void tick() {
            super.tick();
            // Spawn a particle trailing after the spell
            if (this.world.isClient()) {
                double yAdj = this.getY() + this.getHeight()/2;
                this.world.addParticle(ModParticles.PROTO_SPELL_PARTICLE, this.getX(), yAdj, this.getZ(), 0, 0, 0);
            }
        }
    }
}
