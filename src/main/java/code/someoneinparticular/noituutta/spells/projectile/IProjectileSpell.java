package code.someoneinparticular.noituutta.spells.projectile;

import code.someoneinparticular.noituutta.spells.INoituuttaSpell;
import code.someoneinparticular.noituutta.spells.SpellProjectileEntity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.world.World;

import java.util.List;

public interface IProjectileSpell extends INoituuttaSpell {
    SpellProjectileEntity generateEntity(World world, LivingEntity owner, List<INoituuttaSpell> spells);
}
