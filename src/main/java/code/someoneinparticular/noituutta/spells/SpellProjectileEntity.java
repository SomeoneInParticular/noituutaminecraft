package code.someoneinparticular.noituutta.spells;

import com.google.common.base.MoreObjects;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.damage.ProjectileDamageSource;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.ProjectileUtil;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.network.Packet;
import net.minecraft.network.packet.s2c.play.EntitySpawnS2CPacket;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.hit.EntityHitResult;
import net.minecraft.util.hit.HitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.GameRules;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * This is basically a re-code of ProjectileEntity, modified slightly because
 * Mojang made said class's constructor package private for fuck knows why.
 *
 * Object's which extend this class also inherit the "effects" set passively,
 * allowing spell modifiers to be stored and referenced as needed
 */
public abstract class SpellProjectileEntity extends Entity {
    // Entity tracking systems, for attributing damage and kills too
    @Nullable
    private UUID ownerUuid;
    @Nullable
    private Entity owner;

    // Generic projectile entity attributes
    protected float damage = 2.0f;
    protected float knockback = 1.0f;
    protected int life = 50;
    protected boolean hitsOwner = false;
    protected float gravity = 0;

    protected List<TrajectoryModifier> trajectoryModifiers = new ArrayList<>();

    // Default hitbox size for spells
    protected static final float DEFAULT_SPELL_HITBOX_SIZE = 0.2f;

    // -- Constructors -- //
    public SpellProjectileEntity(EntityType<?> type, World world) {
        super(type, world);
    }

    // -- Getters and Setters -- //
    public void setOwner(@Nullable Entity owner) {
        this.owner = owner;
        // Blank the ownerUUID if the owner is null
        if (owner != null) {
            this.ownerUuid = owner.getUuid();
        } else {
            this.ownerUuid = null;
        }
    }

    public @Nullable Entity getOwner() {
        if (this.owner != null && !this.owner.isRemoved()) {
            return this.owner;
        } else if (this.ownerUuid != null && this.world instanceof ServerWorld) {
            this.owner = ((ServerWorld)this.world).getEntity(this.ownerUuid);
            return this.owner;
        } else {
            return null;
        }
    }

    public void addTrajectoryModifier(TrajectoryModifier modifier) {
        this.trajectoryModifiers.add(modifier);
    }

    public TrajectoryModifier getTrajectoryModifier(int index) {
        return this.trajectoryModifiers.get(index);
    }

    public static float getHitboxSize() {
        return DEFAULT_SPELL_HITBOX_SIZE;
    }

    // -- Overridden Functions -- //
    @Override
    protected void writeCustomDataToNbt(NbtCompound nbt) {
        if (this.ownerUuid != null) {
            nbt.putUuid("Owner", this.ownerUuid);
        }
        nbt.putFloat("Damage", damage);
        nbt.putFloat("Knockback", knockback);
        nbt.putBoolean("HitsOwner", hitsOwner);
        nbt.putFloat("Gravity", gravity);
    }

    @Override
    protected void readCustomDataFromNbt(NbtCompound nbt) {
        if (nbt.containsUuid("Owner")) {
            this.ownerUuid = nbt.getUuid("Owner");
        }
        this.damage = nbt.getFloat("Damage");
        this.knockback = nbt.getFloat("Knockback");
        this.hitsOwner = nbt.getBoolean("HitsOwner");
        this.gravity = nbt.getFloat("Gravity");
    }

    @Override
    public void tick() {
        super.tick();

        // Check if we hit anything, and respond if we did
        HitResult hitResult = ProjectileUtil.getCollision(this, this::canHit);
        if (hitResult.getType() == HitResult.Type.BLOCK) {
            this.onBlockHit((BlockHitResult) hitResult);
        } else if (hitResult.getType() == HitResult.Type.ENTITY) {
            this.onEntityHit((EntityHitResult) hitResult);
        }

        // Decrement the life, deleting this entity if it's reached 0
        if (!this.world.isClient()) {
            this.life--;
            if (this.life < 0) {
                this.discard();
                this.onExpired();
            }
        }

        // Move under the effects of gravity, if any
        if (!this.hasNoGravity()) {
            Vec3d newSpeed = this.getVelocity().add(0, -gravity, 0);
            this.setVelocity(newSpeed);
        }

        // Apply any trajectory modifications for this projectile, if any
        if (this.trajectoryModifiers.size() > 0) {
            for (TrajectoryModifier modifier : this.trajectoryModifiers) {
                modifier.shiftTrajectory(this);
            }
            this.velocityDirty = true;
        }

        // Shift the position of the projectile based on it's velocity
        Vec3d posOffset = this.getVelocity();
        double newX = this.getX() + posOffset.getX();
        double newY = this.getY() + posOffset.getY();
        double newZ = this.getZ() + posOffset.getZ();
        this.setPosition(newX, newY, newZ);
    }

    @Override
    public void setVelocityClient(double x, double y, double z) {
        this.setVelocity(x, y, z);
        if (this.prevPitch == 0.0F && this.prevYaw == 0.0F) {
            double d = Math.sqrt(x * x + z * z);
            this.setPitch((float)(MathHelper.atan2(y, d) * 57.2957763671875D));
            this.setYaw((float)(MathHelper.atan2(x, z) * 57.2957763671875D));
            this.prevPitch = this.getPitch();
            this.prevYaw = this.getYaw();
            this.refreshPositionAndAngles(this.getX(), this.getY(), this.getZ(), this.getYaw(), this.getPitch());
        }
    }

    @Override
    public Packet<?> createSpawnPacket() {
        Entity entity = this.getOwner();
        return new EntitySpawnS2CPacket(this, entity == null ? 0 : entity.getId());
    }

    @Override
    public void onSpawnPacket(EntitySpawnS2CPacket packet) {
        super.onSpawnPacket(packet);
        Entity entity = this.world.getEntityById(packet.getEntityData());
        if (entity != null) {
            this.setOwner(entity);
        }
    }

    @Override
    public boolean canModifyAt(World world, BlockPos pos) {
        Entity entity = this.getOwner();
        if (entity instanceof PlayerEntity) {
            return entity.canModifyAt(world, pos);
        } else {
            return entity == null || world.getGameRules().getBoolean(GameRules.DO_MOB_GRIEFING);
        }
    }

    @Override
    public boolean shouldRender(double distance) {
        // This is just a default, and should absolutely be modified with overrides
        return distance < 5000;
    }

    // -- Utility Functions -- //
    /**
     * If this projectile has an owner, attribute anything caused by this
     * projectile to said owner. Otherwise, attribute the projectile to itself
     * @return The owner object, or this projectile if no owner exists
     */
    public Entity getEffectCause() {
        return MoreObjects.firstNonNull(this.getOwner(), this);
    }

    /**
     * Set the velocity explicitly for this projectile, relative to the world
     * @param x Relative x position of vector
     * @param y Relative y position of vector
     * @param z Relative z position of vector
     * @param speed The magnitude of the vector (how fast to zoom in direction indicated prior)
     * @param divergence Some random variation to apply to the velocity
     */
    public void setVelocity(double x, double y, double z, float speed, float divergence) {
        Vec3d vec3d = (new Vec3d(x, y, z)).normalize().add(this.random.nextGaussian() * 0.007499999832361937D * (double)divergence, this.random.nextGaussian() * 0.007499999832361937D * (double)divergence, this.random.nextGaussian() * 0.007499999832361937D * (double)divergence).multiply(speed);
        this.setVelocity(vec3d);
        double d = vec3d.horizontalLength();
        this.setYaw((float)(MathHelper.atan2(vec3d.x, vec3d.z) * 57.2957763671875D));
        this.setPitch((float)(MathHelper.atan2(vec3d.y, d) * 57.2957763671875D));
        this.prevYaw = this.getYaw();
        this.prevPitch = this.getPitch();
    }

    /**
     * Set the properties of this object explicitly, including its orientation and velocity
     * @param user The user to mark as the owner of this projectile
     * @param pitch The pitch the projectile should now have
     * @param yaw The yaw the projectile should now have
     * @param roll The roll the projectile should now have
     * @param speed How fast the projectile should now move
     * @param variance How much random variation should be introduced to
     */
    public void setProperties(Entity user, float pitch, float yaw, float roll, float speed, float variance) {
        float f = -MathHelper.sin(yaw * 0.017453292F) * MathHelper.cos(pitch * 0.017453292F);
        float g = -MathHelper.sin((pitch + roll) * 0.017453292F);
        float h = MathHelper.cos(yaw * 0.017453292F) * MathHelper.cos(pitch * 0.017453292F);
        this.setVelocity(f, g, h, speed, variance);
        Vec3d vec3d = user.getVelocity();
        this.setVelocity(this.getVelocity().add(vec3d.x, user.isOnGround() ? 0.0D : vec3d.y, vec3d.z));
    }

    /**
     * Run the response appropriate to hitting an entity
     * @param entityHitResult The entity hit report
     */
    protected void onEntityHit(EntityHitResult entityHitResult) {
        // Deal any damage and apply any knockback this entity has
        Entity hitEntity = entityHitResult.getEntity();
        DamageSource source = new ProjectileDamageSource("noita_spell", this, this.getOwner());
        if (hitEntity.damage(source, this.damage)) {
            // Apply knock-back to the entity
            Vec3d vector = this.getVelocity().normalize().multiply(this.knockback);
            if (hitEntity.isOnGround() && vector.getY() < 0) {
                vector = vector.multiply(1, -0.1, 1);
            }
            hitEntity.addVelocity(vector.x, vector.y, vector.z);
        }
        // By default, the entity should de-spawn upon hitting an entity
        this.onExpired();
        this.discard();
    }

    /**
     * Run the response appropriate to hitting a block
     * @param blockHitResult The block hit report
     */
    protected void onBlockHit(BlockHitResult blockHitResult) {
        // By default, the entity should de-spawn upon hitting a block
        this.onExpired();
        this.discard();
    }

    /**
     * Run the response appropriate to the projectile expiring
     */
    protected void onExpired() {
    }

    /**
     * Evaluate whether this spell projectile can hit a specified entity
     * @param entity The entity to test against
     * @return Whether this object can hit the specified entity or not
     */
    protected boolean canHit(Entity entity) {
        // Special case for hitting yourself with a spell
        if (entity == this.owner) {
            return this.hitsOwner;
        }
        return !entity.isSpectator() && entity.isAlive() && entity.collides();
    }

    /**
     * Update the rotation of this projectile implicitly
     */
    protected void updateRotation() {
        Vec3d vec3d = this.getVelocity();
        double d = vec3d.horizontalLength();
        this.setPitch(updateRotation(this.prevPitch, (float)(MathHelper.atan2(vec3d.y, d) * 57.2957763671875D)));
        this.setYaw(updateRotation(this.prevYaw, (float)(MathHelper.atan2(vec3d.x, vec3d.z) * 57.2957763671875D)));
    }

    /**
     * Update the rotation of this projectile explicitly
     * @param prevRot The previous rotation this projectile had
     * @param newRot The new rotation this projectile should have
     * @return The updated linear interpolated rotation between the two rotations specified
     */
    protected static float updateRotation(float prevRot, float newRot) {
        // Keep the rotations bounded within a 180 rotation, to prevent wild flailing
        while(newRot - prevRot < -180.0F) {
            prevRot -= 360.0F;
        }
        while(newRot - prevRot >= 180.0F) {
            prevRot += 360.0F;
        }

        // Apply and return the lerp'd rotation value
        return MathHelper.lerp(0.2F, prevRot, newRot);
    }

    // -- Abstract Functions -- //

    /**
     * Get the casting speed of this projectile, for use when a new projectile
     * is spawned. It is bound to a function to allow  for spells to adjust
     * its initial speed based on modifiers or contextual factors.
     * @return The speed the spell projectile should spawn with
     */
    public abstract float getCastSpeed();

    // -- Functional Interfaces -- //
    @FunctionalInterface
    public interface TrajectoryModifier {
        void shiftTrajectory(SpellProjectileEntity entity);
    }
}
