package code.someoneinparticular.noituutta.items;

import code.someoneinparticular.noituutta.Noituutta;
import net.minecraft.item.Item;
import net.minecraft.util.registry.Registry;

public final class ModItems {
    public static final Item PROTO_WAND = new ProtoWand();

    public static void init() {
        Registry.register(Registry.ITEM, Noituutta.generateID("proto_wand"), PROTO_WAND);
    }
}
