package code.someoneinparticular.noituutta.items;

import code.someoneinparticular.noituutta.spells.SpellProjectileEntity;
import code.someoneinparticular.noituutta.spells.modifier.BoomerangSpell;
import code.someoneinparticular.noituutta.spells.projectile.ProtoSpell;
import code.someoneinparticular.noituutta.spells.projectile.SparkBoltSpell;
import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Hand;
import net.minecraft.util.TypedActionResult;
import net.minecraft.world.World;

import java.util.ArrayList;

public class ProtoWand extends WandItem {
    public ProtoWand() {
        super(new FabricItemSettings().group(ItemGroup.MISC));
        this.spellSlots = 1;
        this.spells = new ArrayList<>(3);
        this.spells.add(BoomerangSpell.INSTANCE);
        this.spells.add(SparkBoltSpell.INSTANCE);
        this.spells.add(ProtoSpell.INSTANCE);
    }

    @Override
    public TypedActionResult<ItemStack> use(World world, PlayerEntity user, Hand hand) {
        if (!world.isClient()) {
            SpellProjectileEntity entity = this.castSpell(world, user, this.spells);
            world.spawnEntity(entity);
        }
        return TypedActionResult.success(user.getStackInHand(hand));
    }
}
