package code.someoneinparticular.noituutta.items;

import code.someoneinparticular.noituutta.spells.INoituuttaSpell;
import code.someoneinparticular.noituutta.spells.SpellProjectileEntity;
import code.someoneinparticular.noituutta.spells.projectile.IProjectileSpell;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.Item;
import net.minecraft.world.World;

import java.util.ArrayList;
import java.util.List;

public abstract class WandItem extends Item {
    // TODO; replace with NBT serialized fields
    protected int spellSlots = 5;
    protected List<INoituuttaSpell> spells;

    public WandItem(Settings settings) {
        super(settings);
        spells = new ArrayList<>(spellSlots);
    }

    public SpellProjectileEntity castSpell(World world, LivingEntity owner, List<INoituuttaSpell> spells) {
        // Initialize a list to contain spell modifiers
        List<INoituuttaSpell> queuedModifiers = new ArrayList<>();
        SpellProjectileEntity entity;
        // Iterate through the spell queue, trying to find the first projectile entity
        for (int i = 0; i <= this.spellSlots; i++) {
            INoituuttaSpell spell = spells.get(i);
            if (spell instanceof IProjectileSpell) {
                entity = ((IProjectileSpell) spell).generateEntity(world, owner, queuedModifiers);
                return entity;
            } else {
                queuedModifiers.add(spell);
            }
        }

        return null;
    }
}
