package code.someoneinparticular.noituutta.renderers;

import code.someoneinparticular.noituutta.spells.SpellProjectileEntity;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.render.OverlayTexture;
import net.minecraft.client.render.RenderLayer;
import net.minecraft.client.render.VertexConsumer;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.render.entity.EntityRenderer;
import net.minecraft.client.render.entity.EntityRendererFactory;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.Matrix3f;
import net.minecraft.util.math.Matrix4f;
import net.minecraft.util.math.Vec3f;

@Environment(EnvType.CLIENT)
public class SpellProjectileRenderer extends EntityRenderer<SpellProjectileEntity> {
    // TODO: replace these hard-coded settings with dynamic calls instead
    protected Identifier textureID;

    protected SpellProjectileRenderer(EntityRendererFactory.Context ctx) {
        super(ctx);
    }

    public static SpellProjectileRenderer buildProjectileRenderer(Identifier textureID, EntityRendererFactory.Context ctx) {
        SpellProjectileRenderer renderer = new SpellProjectileRenderer(ctx);
        renderer.textureID = textureID;
        return renderer;
    }

    @Override
    public void render(SpellProjectileEntity entity, float yaw, float tickDelta, MatrixStack matrices, VertexConsumerProvider vertexConsumers, int light) {
        // Push the matrices, preparing it for a new layer
        matrices.push();
        // Scale down the spell to half size
        matrices.scale(0.5F, 0.5F, 0.5F);
        // Rotate the rendering layer towards the player
        matrices.multiply(this.dispatcher.getRotation());
        matrices.multiply(Vec3f.POSITIVE_Y.getDegreesQuaternion(180.0F));

        // Calculate the y offset based on the size of the entities dimensions
        float yOffset = entity.getHeight()/2;

        // Retrieve the model and normal maps for the newest matrix on the stack
        MatrixStack.Entry entry = matrices.peek();
        Matrix4f matrix4f = entry.getModel();
        Matrix3f matrix3f = entry.getNormal();
        // Render the spell's sprite
        RenderLayer layer = RenderLayer.getItemEntityTranslucentCull(this.textureID);
        VertexConsumer vertexConsumer = vertexConsumers.getBuffer(layer);
        produceVertex(vertexConsumer, matrix4f, matrix3f, light, -0.5F, -0.5F+yOffset, 0, 1);
        produceVertex(vertexConsumer, matrix4f, matrix3f, light, 0.5F, -0.5F+yOffset, 1, 1);
        produceVertex(vertexConsumer, matrix4f, matrix3f, light, 0.5F, 0.5F+yOffset, 1, 0);
        produceVertex(vertexConsumer, matrix4f, matrix3f, light, -0.5F, 0.5F+yOffset, 0, 0);
        // Pop the matrices to display our newly rendered layer
        matrices.pop();
        super.render(entity, yaw, tickDelta, matrices, vertexConsumers, light);
    }

    // TODO: replace this hard-coded texture with code to check based on spell projectile type
    @Override
    public Identifier getTexture(SpellProjectileEntity entity) {
        return textureID;
    }

    // Helper function (stolen from DragonFireballEntityRenderer) for drawing
    private static void produceVertex(VertexConsumer vertexConsumer, Matrix4f modelMatrix, Matrix3f normalMatrix, int light, float x, float y, int textureU, int textureV) {
        vertexConsumer.vertex(modelMatrix, x, y, 0F).color(255, 255, 255, 255).texture((float)textureU, (float)textureV).overlay(OverlayTexture.DEFAULT_UV).light(light).normal(normalMatrix, 0.0F, 1.0F, 0.0F).next();
    }
}
