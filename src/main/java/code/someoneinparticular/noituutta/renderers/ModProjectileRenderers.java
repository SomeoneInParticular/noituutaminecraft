package code.someoneinparticular.noituutta.renderers;

import code.someoneinparticular.noituutta.spells.SpellProjectileEntity;
import code.someoneinparticular.noituutta.spells.projectile.ProtoSpell;
import code.someoneinparticular.noituutta.spells.projectile.SparkBoltSpell;
import net.fabricmc.fabric.api.client.rendering.v1.EntityRendererRegistry;
import net.minecraft.entity.EntityType;
import net.minecraft.util.Identifier;

public final class ModProjectileRenderers {
    public static void initClient() {
        // Renderers
        registerSpellRenderer(ProtoSpell.PROJECTILE_TYPE, ProtoSpell.TEXTURE);
        registerSpellRenderer(SparkBoltSpell.PROJECTILE_TYPE, SparkBoltSpell.TEXTURE);
    }

    public static <E extends SpellProjectileEntity> void registerSpellRenderer(EntityType<? extends E> entityType, Identifier texture) {
        EntityRendererRegistry.register(entityType, ctx -> SpellProjectileRenderer.buildProjectileRenderer(texture, ctx));
    }
}
