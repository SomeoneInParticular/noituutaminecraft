package code.someoneinparticular.noituutta;

import net.minecraft.entity.EntityDimensions;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;

public final class SpellUtils {
    public static Vec3d targetedAcceleration(Vec3d from, Vec3d to, float r) {
        // Convert the values into a set of radial co-ordinates (using y as our "opposite" axis)
        Vec3d delta = to.subtract(from);
        float phiXY = (float) MathHelper.atan2(delta.y, delta.x);
        float phiZY = (float) MathHelper.atan2(delta.y, delta.z);
        // Calculate the acceleration vector components along each axis
        double x = r * MathHelper.cos(phiXY);
        double y = r * MathHelper.sin(phiXY);
        double z = r * MathHelper.cos(phiZY);
        // Calculate the new velocity
        return new Vec3d(x, y, z);
    }

    /**
     * Returns a perfect cubic set of entity dimensions
     * @param length The length along the side of the resulting cube
     * @return An EntityDimension instance representing a cube
     */
    public static EntityDimensions cubeEntity(float length) {
        return EntityDimensions.fixed(length, length);
    }
}
