package code.someoneinparticular.noituutta;

import code.someoneinparticular.noituutta.items.ModItems;
import code.someoneinparticular.noituutta.particles.ModParticles;
import net.fabricmc.api.ModInitializer;
import net.minecraft.util.Identifier;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Noituutta implements ModInitializer {
	// Mod identifier space
	public static final String MOD_ID = "noituutta";
	// Mod logger
	public static final Logger LOGGER = LogManager.getLogger("noituutta");

	public static Identifier generateID(String namespace) {
		return new Identifier(MOD_ID, namespace);
	}

	@Override
	public void onInitialize() {
		LOGGER.info("Loading Noituutta Mod (Server-Side)");
		ModItems.init();
		ModParticles.init();
		LOGGER.info("Finished Loading Noituutta Mod (Server-Side)!");
	}
}
