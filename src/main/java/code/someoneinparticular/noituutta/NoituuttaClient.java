package code.someoneinparticular.noituutta;

import code.someoneinparticular.noituutta.renderers.ModProjectileRenderers;
import code.someoneinparticular.noituutta.particles.ModParticles;
import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;

@Environment(EnvType.CLIENT)
public class NoituuttaClient implements ClientModInitializer {
    @Override
    public void onInitializeClient() {
        ModProjectileRenderers.initClient();
        ModParticles.initClient();
    }
}
