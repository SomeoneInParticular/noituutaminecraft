package code.someoneinparticular.noituutta.particles;

import code.someoneinparticular.noituutta.Noituutta;
import net.fabricmc.fabric.api.client.particle.v1.ParticleFactoryRegistry;
import net.fabricmc.fabric.api.event.client.ClientSpriteRegistryCallback;
import net.fabricmc.fabric.api.particle.v1.FabricParticleTypes;
import net.minecraft.particle.DefaultParticleType;
import net.minecraft.particle.ParticleEffect;
import net.minecraft.particle.ParticleType;
import net.minecraft.screen.PlayerScreenHandler;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public final class ModParticles {
    public static final DefaultParticleType PROTO_SPELL_PARTICLE = FabricParticleTypes.simple(true);

    private static void register(String name, DefaultParticleType particleType) {
        Identifier id = Noituutta.generateID(name);
        Registry.register(Registry.PARTICLE_TYPE, id, particleType);
    }

    private static <T extends ParticleEffect> void registerClient(String name, ParticleType<T> type, ParticleFactoryRegistry.PendingParticleFactory<T> factory) {
        Identifier id = Noituutta.generateID(name);
        ClientSpriteRegistryCallback.event(PlayerScreenHandler.BLOCK_ATLAS_TEXTURE).register(((atlasTexture, registry) -> registry.register(id)));
        ParticleFactoryRegistry.getInstance().register(type, factory);
    }

    public static void init() {
        register("proto_spell", PROTO_SPELL_PARTICLE);
    }

    public static void initClient() {
        registerClient("particle/proto_spell", PROTO_SPELL_PARTICLE, ProtoSpellParticle.Factory::new);
    }
}
